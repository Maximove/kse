//= ../../node_modules/jquery/dist/jquery.js
/*//= ../../node_modules/selectric/public/jquery.selectric.js*/
//= ../../node_modules/swiper/dist/js/swiper.js
//= ../../node_modules/nouislider/distribute/nouislider.js
var mySwiper = new Swiper ('.section-great-power-left-swiper-container', {
    // Optional parameters
    slidesPerView: 1,
    loop: true,
    breakpoints: {
        // when window width is <= 320px
        320: {
            centeredSlides: true,
            slidesPerView: 1
        }
    },
    pagination: {
        el: '.swiper-pagination',
    }
});
////////////
$(document).ready(function () {
    $('.section-study-forms-menu-wrap-content').hide();
    $('.section-study-forms-menu-toggle_menu-click').on('click', function(){
        $('.section-study-forms-menu-wrap-content').slideToggle();
    });
});
///////////
var slider = document.getElementById('slider');

noUiSlider.create(slider, {
    start: [0, 80],
    step: 20,
    connect: true,
    range: {
        'min': 0,
        'max': 80
    },
    pips: {
        mode: 'steps',
        stepped: true,
        density: 4
    }
});
